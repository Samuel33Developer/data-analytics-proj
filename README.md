1. Austin_Animal_Center_Outcomes.csv - The raw original dataset Used for the project 
2. dataset.csv - The Product dataset created after performing data cleaning and data engineering on Austin_Animal_Center_Outcomes.csv
3. dataset_test.csv - The test data utilized by Darwin after splitting dataset.csv into test/training data 
4. dataset_train.csv - The train data utilized by Darwin after splitting dataset.csv into test/training data to be utilized for Darwin's model building
5. Paper.html - Our written report that was created on Jupyter Notebook, exported for html
6. Paper.ipynb - Our written report created on Jupyter Notebook
7. Project.html - Our project where we conducted our data cleaning, data engineering, and model building with darwin, exported for html
8. Project.ipynb - Our project where we conducted our data cleaning, data engineering, and model building with darwin on Jupyter Notebook
9. Test_Accuracy.png - A picture used in our Paper report thats depictive of our test accuracy
10. Train_Accuracy.png - A picture used in our Paper report thats depictive of our train accuracy
11. Miscellaneous - Files that are not needed for review. (Extras)